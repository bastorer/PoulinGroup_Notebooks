{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Expand the view to scale with window width\n",
    "from IPython.core.display import display, HTML\n",
    "display(HTML(\"<style>.container { width:90% !important; }</style>\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1 align=\"center\">\n",
    "ELMS3a\n",
    "</h1>\n",
    "<h2 align=\"center\">\n",
    "Two-Dimensional Diffusion Equation\n",
    "</h2>\n",
    "<h3 align=\"center\">\n",
    "By Francis J. Poulin, Adam Morgan, and Ben Storer\n",
    "</h3>\n",
    "\n",
    "## Model Equations ##\n",
    "The two-dimensional (2D) diffusion equation with variable diffusivity $K(x,y)$ is\n",
    "$$\n",
    "\\frac{\\partial u}{\\partial t} = \\nabla\\cdot \\left(K(x,y) \\ \\nabla u\\right).\n",
    "$$\n",
    "This is a **Parabolic PDE** of the Sturm-Liouville type with \n",
    "\\begin{align*}\n",
    "\\rho(x,y) &= 1,\\\\\n",
    "p(x,y) &= K(x,y), \\\\ \n",
    "q(x,y) &= 0.\n",
    "\\end{align*}\n",
    "\n",
    "Our spatial domain is the rectangle $\\Omega= [0,L_{x}]\\times [0,L_{y}]$. We have the option to impose two sets of boundary conditions and one initial condition.\n",
    "\n",
    "### Dirichlet: ###\n",
    "$$\n",
    "u(0,y,t) = 0,\n",
    "\\quad \\mbox{ and } \\quad\n",
    "u(L_{x},y,t) = 0\n",
    "\\quad \\mbox{ and } \\quad\n",
    "u(x,0,t) = 0\n",
    "\\quad \\mbox{ and } \\quad\n",
    "u(x,L_{y},t) = 0.\n",
    "$$\n",
    "\n",
    "Succinctly, $u|_{\\partial \\Omega}=0$. The code can be easily modified to relax homogeneity of the BCs. \n",
    "### Neumann: ###\n",
    "Letting $\\mathbf{n}$ denote the unit outward normal on the boundary $\\partial \\Omega$, \n",
    "$$\n",
    "\\nabla u\\cdot \\mathbf{n}\\ (0,y,t) = 0,\n",
    "\\quad \\mbox{ and } \\quad\n",
    "\\nabla u\\cdot \\mathbf{n}\\ (L_{x},y,t) = 0\n",
    "\\quad \\mbox{ and } \\quad\n",
    "\\nabla u\\cdot \\mathbf{n}\\ (x,0,t) = 0\n",
    "\\quad \\mbox{ and } \\quad\n",
    "\\nabla u\\cdot \\mathbf{n}\\ (x,L_{y},t) = 0.\n",
    "$$\n",
    "\n",
    "We could also say that $\\left(\\nabla u \\cdot \\mathbf{n}\\right)|_{\\partial \\Omega}=0$.\n",
    "### Initial Condition ###\n",
    "$$\n",
    "u(x,y,0) = f(x,y).\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Weak Form ##\n",
    "\n",
    "Since the PDE involves both spatial and temporal derivatives, we must find a way to approximate each. Our approach is to use a FD method in time ($2^{\\mathrm{nd}}$ order Crank-Nickolson) and a FE method in space ($1^{\\mathrm{st}}$ order).\n",
    "\n",
    "#### Temporal Discretization ####\n",
    "\n",
    "If we define $u_n(x,y) = u(x, y, t_n)$ then our time-stepping scheme is\n",
    "$$\n",
    "u^{n+1}  = u^n + \\Delta t\n",
    "\\left[ \\nabla\\cdot \\left(K(x,y)\\ \\nabla\\left( \\frac{u^{n+1} + u^n}{2} \\right) \\right)\\right]\n",
    "$$\n",
    "\n",
    "#### Spatial Approximation ####\n",
    "\n",
    "Next, we obtain the weak form of this equation by multiplying it by a test function $v$ to obtain the new solution $u^*$ in terms of the known solution $u^n$.\n",
    "\\begin{align*}\n",
    "\\int_{\\Omega} v u^* \\, dx  \n",
    "& = \\int_{\\Omega} v u^n \\, dx + \\Delta t\n",
    "\\int_{\\Omega} v \\left[ \\nabla \\left(K(x,y) \\ \\nabla\\left( \\frac{u^* + u^n}{2} \\right) \\right)\\right] \\, dx, \\\\\n",
    "\\int_{\\Omega} v u^* \\, dx & = \\int_{\\Omega} v u^n \\, dx \n",
    "- \\Delta t \\int_{\\Omega} K(x,y) \\ \\left(\\nabla\\left( \\frac{u^* + u^n}{2} \\right) \\cdot \\nabla v\\right) \\, dx\n",
    "+ \\Delta t  \\oint_{\\partial \\Omega} K(x,y) \\ v \\ \\nabla \\left( \\frac{u^* + u^n}{2} \\right) \\cdot \\mathbf{n} \\ \\mathrm{d} s\n",
    "\\end{align*}\n",
    "If we are using either Dirichlet or homogeneous Neumann boundary conditions, then the boundary term vanishes (in the former case, this is because the test functions are chosen to vanish) and we obtain the final weak form\n",
    "\\begin{align*}\n",
    "\\int_{\\Omega} v u^*  + \\frac{\\Delta t}{2}  K(x,y) \\ \\nabla u^{*}\\cdot\\nabla v \\, dx & =  \\int_{\\Omega} v u^n  \n",
    "- \\frac{\\Delta t}{2}  K(x,y) \\ \\nabla u^{n}\\cdot\\nabla v \\, dx.\n",
    "\\end{align*}\n",
    "The left hand side is a bilinear form, which we denote by $a(u^{*},v)$. The right hand side is a linear form, which we denote with $L(v)$. In this more compact notation, the weak form of the PDE at each time step becomes this: find $u^{*}$ such that the equation\n",
    "\\begin{align*}\n",
    "a(u^{*},v) & = L(v)\n",
    "\\end{align*}\n",
    "is satisfied for all differentiable functions $v$ vanishing on the boundary. At each time step we know the solution $u^n$. Then, we solve the above linear system for the new solution, $u^*$, and that is our $u^{n+1}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Numerical Solution#\n",
    "\n",
    "Below we go through each step of the numerical method to solve the above weak form.\n",
    "\n",
    "#### Import Libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import firedrake as fdr\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.animation as animation\n",
    "import cmocean\n",
    "from IPython.display import HTML\n",
    "import sys"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specify parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T  = 2.0          # Final time\n",
    "dt = 0.02         # Time step \n",
    "Nt = np.int(T/dt) # Number of time steps\n",
    "Dt = fdr.Constant(dt)\n",
    "\n",
    "Bdry = 'Dirichlet'\n",
    "#Bdry = 'Neumann'\n",
    "\n",
    "Lx = 6.0          # Length of domain\n",
    "Nx = 20          # Number of elements\n",
    "\n",
    "Ly = 2.0          # Length of domain\n",
    "Ny = 20          # Number of elements\n",
    "\n",
    "deg  = 2          # Order of approximation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create the grid"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Time\n",
    "tt = np.linspace(0., T, Nt)\n",
    "\n",
    "# Space\n",
    "mesh  = fdr.RectangleMesh(Nx, Ny, Lx, Ly)\n",
    "x,y   = fdr.SpatialCoordinate(mesh)\n",
    "\n",
    "dx = Lx/Nx\n",
    "dy = Ly/Ny\n",
    "xvec  = np.arange(dx/2., Lx, dx)\n",
    "yvec  = np.arange(dy/2., Ly, dy)\n",
    "\n",
    "# This will be helpful for pcolor plots\n",
    "xplot = np.arange(0, Lx+dx, dx)\n",
    "yplot = np.arange(0, Ly+dy, dy)\n",
    "\n",
    "[xmat,ymat] = np.meshgrid(xvec, yvec, indexing='ij') # Helps create contour plots\n",
    "[xplotmat, yplotmat] = np.meshgrid(xplot, yplot, indexing='ij')\n",
    "\n",
    "\n",
    "# Define Function Space\n",
    "V = fdr.FunctionSpace(mesh, \"CG\", deg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "fdr.plot(mesh)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Help?\n",
    "\n",
    "I need to find a way to export a Firedrake function, say K below, to a grid, to be able to plot this in matplotlib.\n",
    "\n",
    "The following gives info on at:\n",
    "\n",
    "https://www.firedrakeproject.org/point-evaluation.html\n",
    "\n",
    "I asked on slack and Lawrence told me the following:\n",
    "\n",
    "    you need to turn the `xmat` `ymat` arrays into a single array of `(x, y)` points\n",
    "    \n",
    "    `.at` takes a list of n-D points so that's what you need to turn your arrays into somehow\n",
    "    \n",
    "Ben: since you are the one that understands python much better than I can might you be able to figure this out?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Provide the diffusivity function $K(x)$\n",
    "Opt0: $ 1 $\n",
    "\n",
    "Opt1: $ \\exp\\left[ -5\\left(x-\\frac{L_{x}}{2}\\right)^2 -5\\left(y-\\frac{L_{y}}{2}\\right)^2 \\right] $\n",
    "\n",
    "Opt2: $ 1 + 2\\left(\\frac{x}{L_{x}} - 0.5\\right)$\n",
    "\n",
    "Opt3: $1 + \\tanh\\left[10 \\cdot \\left(\\frac{x}{L_{x}} - \\frac{1}{2}\\right)\\right] $\n",
    "\n",
    "Opt4: $1 + 0.5\\sin\\left[ 2 \\pi \\left( x - \\frac{L_{x}}{2}\\right) / \\frac{L_{x}}{2} \\right]$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "K = fdr.Function(V)\n",
    "\n",
    "#FJP: rewrite this?\n",
    "Opt = 3\n",
    "if Opt==0: \n",
    "    K.interpolate(fdr.Expression(\" 1 + 0*x[0]/Lx - 0*x[1]/Ly\", Lx=Lx, Ly=Ly));    \n",
    "elif Opt==1:\n",
    "    K.interpolate(fdr.Expression(\" exp(-5*(pow(x[0]-Lx/2.,2)+pow(x[1]-Ly/2.,2)))\", Lx=Lx, Ly=Ly));\n",
    "elif Opt==2:\n",
    "    K.interpolate(fdr.Expression(\" 1 + (x[0]-Lx/2)/Lx*2\", Lx=Lx));\n",
    "elif Opt==3:\n",
    "    K.interpolate(fdr.Expression(\" 1 + 0.9*tanh((x[0] - Lx/2)/(Lx/10))\", Lx=Lx));\n",
    "elif Opt==4:\n",
    "    K.interpolate(fdr.Expression(\" 1 + 0.9*sin(2*pi*(x[0] - Lx/2)/(Lx/2))\", Lx=Lx));\n",
    "else:\n",
    "    printf(\"Must choose a valid option to set diffusivity\")\n",
    "    sys.exit()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot Diffusivity \n",
    "\n",
    "Note that here we are using the firedrake-specific plotting features,\n",
    "since we are plotting a Firedrake object. Under the hood it is still using\n",
    "matplotlib, but it is tucked away."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract K into a 2D numpy array. It's a little messy, but gets the job done.\n",
    "#     Notes: \n",
    "#         0. this uses the python iterator notation, which can be very powerful\n",
    "#              the general structure is:   my_list = [f(x) for x in another_list]\n",
    "#              this allows to you create a new list from a previous list, without\n",
    "#              paying the price of 'for' loops\n",
    "#         1. What is zip? Zip is a convenient way of combining two lists into one two-column list\n",
    "#              It's primary use is in iterables such as this, or in loop statements.\n",
    "#         2. So, in this example we create a list of K 'at' each 'point', where\n",
    "#              point comes from a zipped list of all of the points\n",
    "#         3. Afterwards, we convert it into a numpy array then the put it into the right shape.\n",
    "                          \n",
    "k_list = [K.at(point, tolerance=1e-12) for point in zip(xmat.ravel(), ymat.ravel())]\n",
    "kmat = np.array(k_list).reshape(xmat.shape)\n",
    "\n",
    "# Option for contours\n",
    "#CS = plt.contourf(xmat, ymat, kmat, 32, cmap = cmocean.cm.thermal)\n",
    "\n",
    "# Option for pcolor\n",
    "plt.pcolormesh(xplotmat, yplotmat, kmat, \n",
    "              cmap = cmocean.cm.thermal,\n",
    "              edgecolor = 'k', linewidth = 0.01)\n",
    "\n",
    "plt.colorbar()\n",
    "plt.title('Diffusion Coefficient K(x,y)')\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set the boundary conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# FJP: Rewrite this\n",
    "\n",
    "if Bdry == 'Dirichlet':\n",
    "    \n",
    "    #Remark: The DirichletBC command needs three arguments: \n",
    "    # function space, the value and which boundary endpoint \n",
    "    # Note: (\"1\" for left and \"2\" for right)    \n",
    "    \n",
    "    bcLeft   = fdr.DirichletBC(V, fdr.Constant(0.0), 1)\n",
    "    bcRight  = fdr.DirichletBC(V, fdr.Constant(0.0), 2)\n",
    "    bcBottom = fdr.DirichletBC(V, fdr.Constant(0.0), 3)\n",
    "    bcUp     = fdr.DirichletBC(V, fdr.Constant(0.0), 4)\n",
    "\n",
    "    bcs = [bcLeft, bcRight, bcBottom, bcUp]\n",
    "    \n",
    "elif Bdry == 'Neumann':\n",
    "\n",
    "    bcs = []\n",
    "    \n",
    "else:\n",
    "\n",
    "    print('Must select Dirichlet or Neumann boundary conditions')\n",
    "    sys.exit()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Specify initial conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define solutions\n",
    "u0 = fdr.Function(V)    \n",
    "u1 = fdr.Function(V)    \n",
    "\n",
    "# Pick initial conditions\n",
    "u0.interpolate(fdr.Expression(\"exp(-1*pow(x[0]-Lx/2.,2)-1*pow(x[1]-Ly/2.,2))\", Lx=Lx, Ly=Ly));\n",
    "\n",
    "# Constant: interesting with Dirichlet BCs\n",
    "#u0.interpolate(fdr.Expression(\"1.0\"));"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot the inital conditions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Store values of u0 as a numpy array\n",
    "u0_list = [u0.at(point, tolerance=1e-12) for point in zip(xmat.ravel(), ymat.ravel())]\n",
    "u0mat = np.array(u0_list).reshape(xmat.shape)\n",
    "\n",
    "# Option for contour\n",
    "#plt.contourf(  xmat, ymat, u0mat, 32, vmin=0, vmax=1, cmap = cmocean.cm.thermal)\n",
    "\n",
    "# Option for pcolor\n",
    "plt.pcolormesh(xplotmat, yplotmat, u0mat, \n",
    "               vmin=0, vmax=1,                     # specify bounds for colour bar\n",
    "               cmap = cmocean.cm.thermal,          # specify colour map\n",
    "               edgecolor='k', linewidth=0.01)      # Add grid-lines to see resolution\n",
    "\n",
    "plt.colorbar()\n",
    "plt.title('Initial Conditions')\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Step_Diffusion(u0, K, bcs):\n",
    "    \n",
    "    # Define functions to build weak form\n",
    "    v = fdr.TestFunction(V)\n",
    "    u = fdr.TrialFunction(V)\n",
    "    \n",
    "    # Define weak form\n",
    "    # \"a\" is a bilinear form: a(u,v)\n",
    "    # \"L\" is a linear form:   L(v)\n",
    "    a = (v*u  + 0.5*K*Dt*( fdr.inner(fdr.grad(u), fdr.grad(v))) )*fdr.dx\n",
    "    L = (v*u0 - 0.5*K*Dt*( fdr.inner(fdr.grad(u0),fdr.grad(v))) )*fdr.dx  \n",
    "\n",
    "    # Solve weak form\n",
    "    fdr.solve(a == L, u1, solver_parameters={'mat_type': 'aij','ksp_type': 'preonly','pc_type': 'lu'}, bcs = bcs)\n",
    "\n",
    "    return u1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Create arrays to store the solution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "usoln      = np.zeros((Nt,Nx,Ny))\n",
    "usoln[0,:,:] = u0mat \n",
    "\n",
    "mass = np.zeros(Nt)\n",
    "mass[0] = fdr.assemble(u0*fdr.dx)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Loop over the time-steps, storing the solution as we go"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for cnt in range(1,Nt):\n",
    "    \n",
    "    t = cnt*dt\n",
    "    \n",
    "    u1 = Step_Diffusion(u0, K, bcs)\n",
    "    \n",
    "    u0.assign(u1)\n",
    "    mass[cnt] = fdr.assemble(u0*fdr.dx)\n",
    "    \n",
    "    u0_list = [u0.at(point, tolerance=1e-12) for point in zip(xmat.ravel(), ymat.ravel())]\n",
    "    u0mat = np.array(u0_list).reshape(xmat.shape)\n",
    "\n",
    "    usoln[cnt,:,:] = u0mat "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot mass and relative change\n",
    "\n",
    "Note that in the case of Dirichlet boundary conditions, we do not expect mass/energy conservation. To see this, consider the long-term behaviour of the system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute relative change\n",
    "relerror = (mass[0] - mass)/mass[0]\n",
    "\n",
    "# Create figure and two y-axes\n",
    "plt.figure()\n",
    "ax1 = plt.gca()\n",
    "ax2 = ax1.twinx()\n",
    "\n",
    "# Plot the mass and relative change on the two axes\n",
    "ax1.plot(tt, mass,     'b', lw=2, label='mass')\n",
    "ax2.plot(tt, relerror, 'r', lw=2, label='relative error')\n",
    "\n",
    "# Specify axis labels\n",
    "ax1.set_xlabel('time')\n",
    "ax1.set_ylabel('Mass (blue)')\n",
    "ax2.set_ylabel('Relative change (red)')\n",
    "\n",
    "# Add a grid\n",
    "plt.grid('on')\n",
    "\n",
    "# Fix some layout stuff\n",
    "plt.tight_layout(True)\n",
    "\n",
    "# Add legend\n",
    "#plt.legend(loc='best')\n",
    "\n",
    "# Not strictly necessary in iPython\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### System State at Fixed Time"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Pick what time step we want to plot\n",
    "cnt_plot = 0 \n",
    "        \n",
    "plt.figure()\n",
    "\n",
    "# Option for contour\n",
    "#CS = plt.contourf(xmat, ymat, usoln[cnt_plot][:][:], 32,\n",
    "#                  vmin=0, vmax=1,\n",
    "#                  cmap = cmocean.cm.thermal)\n",
    "\n",
    "# Option for pcolor\n",
    "CS = plt.pcolormesh(xplotmat, yplotmat, usoln[cnt_plot][:][:],\n",
    "                    vmin=0, vmax=1,\n",
    "                    cmap = cmocean.cm.thermal,\n",
    "                    edgecolor='k', linewidth=0.01)\n",
    "plt.colorbar()\n",
    "plt.xlabel(\"x\")\n",
    "plt.ylabel(\"y\")\n",
    "plt.title('Contour Plot of u at t=%f'%(dt*cnt_plot))\n",
    "plt.show()\n",
    "\n",
    "#If you want to explort a figure to include in your writeup comment out the above line and uncomment the line below.\n",
    "#plt.savefig('tmp.png')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create animations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure()\n",
    "\n",
    "ax  = fig.add_axes([0.1, 0.1, 0.75, 0.8])\n",
    "cax = fig.add_axes([0.9, 0.1, 0.05, 0.8])\n",
    "\n",
    "# animation function\n",
    "def animate(i): \n",
    "    ax.clear()\n",
    "    cax.clear()\n",
    "    z = usoln[i,:,:]\n",
    "    \n",
    "    # Option for contours\n",
    "    #cs = ax.contourf(xmat, ymat, z, np.linspace(0, 1, 32),\n",
    "    #                 cmap = cmocean.cm.thermal)\n",
    "    #fig.colorbar(cs, cax=cax, ax=ax)\n",
    "    \n",
    "    # Option for pcolor\n",
    "    qm = ax.pcolormesh(xplotmat, yplotmat, \n",
    "                       z, vmin=0, vmax=1,\n",
    "                       cmap = cmocean.cm.thermal,\n",
    "                       edgecolor='k', linewidth=0.01)\n",
    "    fig.colorbar(qm, cax=cax, ax=ax)\n",
    "    \n",
    "    ax.set_title('Temperature at t=%f'%(dt*i)) \n",
    "    ax.set_xlabel('x')\n",
    "    ax.set_ylabel('y')\n",
    "    \n",
    "\n",
    "    return ax \n",
    "\n",
    "# Notice how I set the lower colour limit of the contour plot to be negative, which is unphysical. \n",
    "# I made this choice because occasionally numerical errors cause the solution to drop below zero \n",
    "# at a few grid points near the boundary, and this can in turn cause weird blobs to \n",
    "# appear in the picture. 99% of the time with this routine, however, setting the lower limit to 0 is okay, \n",
    "# I just didn't want anyone to panic if they saw odd spots in their picture. \n",
    "\n",
    "anim = animation.FuncAnimation(fig, animate, frames=Nt, blit=False) \n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HTML(anim.to_html5_video())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anim.save('2D_diffusion_VarConductivity.mp4', fps=15, extra_args=['-vcodec', 'libx264'])"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
